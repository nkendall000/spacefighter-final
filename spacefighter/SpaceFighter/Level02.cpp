

#include "Level02.h"
#include "GameplayScreen.h"
#include "BioEnemyShip.h"

void Level02::changeLevel(ResourceManager *pResourceManager)
{
	GameplayScreen *nextLevel = new GameplayScreen(2);
	nextLevel->LoadContent(pResourceManager);
}

void Level02::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 42;

	double xPositions[COUNT] =
	{
		0.15, 0.5, 0.8,
		0.55, 0.45, 0.9,
		0.05, 0.15, 0.5, 0.7, 0.85,
		0.7, 0.75, 0.65, 0.8, 0.9,
		0.5, 0.4, 0.3, 0.45, 0.25,

		0.1, 0.9,
		0.9, 0.85, 0.2,
		0.35, 0.45, 0.65, 0.75,
		0.05, 0.15, 0.95, 0.85,
		0.5,
		0.35, 0.45, 0.5, 0.55, 0.65,
		0.25, 0.75
	};

	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.45, 0.25, 0.25, 0.25,
		3.25, 0.15, 0.35, 0.35, 0.35,
		3.5, 0.4, 0.4, 0.1, 0.1,

		2.5, 0.35,
		3.5, 0.35, 0.35,
		4.0, 0.25, 0.25, 0.15,
		4.0, 0.25, 0.25, 0.15,
		2.5,
		4.5, 0.25, 0.25, 0.15, 0.15,
		3.5, 0.25


	};

	float delay = 2.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
		cap--;
	}

	Level::LoadContent(pResourceManager);

	while (cap > -1)
	{
		if (cap == 0) changeLevel(pResourceManager);

		cap--;
	}
}

