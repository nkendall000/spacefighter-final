
#pragma once

#include "EnemyShip.h"

class BossEnemyShip : public EnemyShip
{

public:

	BossEnemyShip();
	virtual ~BossEnemyShip() { }

	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);


private:

	Texture *m_pTexture;

};