

#include "Level03.h"
#include "BioEnemyShip.h"
#include "BossEnemyShip.h"


void Level03::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BossEnemyShip.png");

	const int COUNT = 1;

	double xPositions[COUNT] =
	{
		0.5
	};

	double delays[COUNT] =
	{
		1.5
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BossEnemyShip *pEnemy = new BossEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

