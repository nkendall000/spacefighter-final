
#include "GameplayScreen.h"
#include "Level01.h"
#include "Level02.h"
#include "BioEnemyShip.h"

void Level01::changeLevel(ResourceManager *pResourceManager)
{
	GameplayScreen *nextLevel = new GameplayScreen(1);
	nextLevel->LoadContent(pResourceManager);
	

}


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 21;


	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	double delays[COUNT] =
	{
		0.0, 0.35, 0.35,
		4.0, 0.35, 0.35,
		4.25, 0.55, 0.55, 0.55, 0.55,
		4.25, 0.25, 0.25, 0.25, 0.25,
		4.5, 0.6, 0.6, 0.3, 0.3
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
		cap--;
	}

	Level::LoadContent(pResourceManager);
	while (cap > -1)
	{
		if (cap == 0) changeLevel(pResourceManager);

		cap--;
	}

}

